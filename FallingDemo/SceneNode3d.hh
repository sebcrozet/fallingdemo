#ifndef SCENENODE3d
# define SCENENODE3d

# include <irrlicht/irrlicht.h>
# include <Falling3d/Aliases.hh>

using namespace irr;
using namespace core;
using namespace scene;
using namespace video;
using namespace io;

class SceneNode3d
{
  protected:
    ISceneNode*        _sceneNode    = nullptr;
    ISceneManager*     _sceneManager = nullptr;
    Falling3d::Body3d* _physics      = nullptr;

  public:
    SceneNode3d(ISceneManager* manager)
    {
      _sceneManager = manager;
    }

    virtual void draw()
    {
      if (_sceneNode)
      {
        const Falling3d::Vect3d& position =
          Falling::Algebra::translation(Falling::Body::localToWorld(*_physics));

        _sceneNode->setPosition(vector3df(position[0], position[1], position[2]));
      }
    }

    virtual Falling3d::Body3d* physics()
    { return _physics; }

    virtual ~SceneNode3d()
    {
      if (_physics)
        delete _physics;
    }
};

#endif // TEST_SCENENODE3d
