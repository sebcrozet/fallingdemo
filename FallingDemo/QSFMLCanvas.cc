
////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <iostream>
#include "QSFMLCanvas.hh"

// Platform-specific headers
#ifdef Q_WS_X11
#include <Qt/qx11info_x11.h>
#include <X11/Xlib.h>
#endif


////////////////////////////////////////////////////////////
/// Construct the QSFMLCanvas
////////////////////////////////////////////////////////////
QSFMLCanvas::QSFMLCanvas(
    QWidget*        Parent
    , const QPoint& Position
    , const QSize&  Size
    , unsigned int  FrameTime) :
  QWidget       (Parent),
  myInitialized (false)
{
  // Setup some states to allow direct rendering into the widget
  setAttribute(Qt::WA_PaintOnScreen);
  setAttribute(Qt::WA_OpaquePaintEvent);
  setAttribute(Qt::WA_NoSystemBackground);

  // Set strong focus to enable keyboard events to be received
  setFocusPolicy(Qt::StrongFocus);

  // Setup the widget geometry
  move(Position);
  resize(Size);

  // Setup the timer
  myTimer.setInterval(FrameTime);
}


////////////////////////////////////////////////////////////
/// Destructor
////////////////////////////////////////////////////////////
QSFMLCanvas::~QSFMLCanvas()
{
  // Nothing to do...
}


////////////////////////////////////////////////////////////
/// Notification for the derived class that moment is good
/// for doing initializations
////////////////////////////////////////////////////////////
void QSFMLCanvas::OnInit()
{
  // Nothing to do by default...
}


////////////////////////////////////////////////////////////
/// Notification for the derived class that moment is good
/// for doing its update and drawing stuff
////////////////////////////////////////////////////////////
void QSFMLCanvas::OnUpdate()
{
  // Nothing to do by default...
}


////////////////////////////////////////////////////////////
/// Return the paint engine used by the widget to draw itself
////////////////////////////////////////////////////////////
QPaintEngine* QSFMLCanvas::paintEngine() const
{
  return 0;
}


////////////////////////////////////////////////////////////
/// Called when the widget is shown ;
/// we use it to initialize our SFML window
////////////////////////////////////////////////////////////
void QSFMLCanvas::showEvent(QShowEvent*)
{
  // Create the SFML window with the widget handle
  // FIXME: this is uggly to do this each time (whenever we switch between 2D and 3D view) but it
  // works. There might be a better way to do this though.
  RenderWindow::create(winId());

  // Let the derived class do its specific stuff
  OnInit();

  if (!myInitialized)
  {
    // Setup the timer to trigger a refresh at specified framerate
    connect(&myTimer, SIGNAL(timeout()), this, SLOT(repaint()));

    myInitialized = true;
  }

  setView(_view);
}

void QSFMLCanvas::resizeEvent(QResizeEvent* event)
{
  _view.setSize(event->size().width(), event->size().height());
  setView(_view);
}

void QSFMLCanvas::wheelEvent(QWheelEvent* event)
{
  const double zoomFactor = 0.1;

  double delta = (double)event->delta() / 120.0; // between -1.0 and 1.0
  _currentZoom *= (1.0 + delta * zoomFactor);
  _view.zoom(1.0 + delta * zoomFactor);

  setView(_view);
}

void QSFMLCanvas::mousePressEvent(QMouseEvent* event)
{
  _lastX = event->x();
  _lastY = event->y();
}

void QSFMLCanvas::mouseMoveEvent(QMouseEvent* event)
{
  double zoom = std::abs(_currentZoom);

  _view.move((_lastX - event->x()) * zoom, (_lastY - event->y()) * zoom);

  setView(_view);

  _lastX = event->x();
  _lastY = event->y();
}


////////////////////////////////////////////////////////////
/// Called when the widget needs to be painted ;
/// we use it to display a new frame
////////////////////////////////////////////////////////////
void QSFMLCanvas::paintEvent(QPaintEvent*)
{
  setActive(true);
  // Let the derived class do its specific stuff
  OnUpdate();

  // Display on screen
  display();
}
