#ifndef SCENARIOMANAGER
# define SCENARIOMANAGER

# include <QObject>
# include "Scenario.hh"

class MyCanvas;
class MyCanvas3d;

class ScenarioManager : public QObject
{
  Q_OBJECT

  private:
    Scenario*              _current;
    std::vector<Scenario*> _all;
    MyCanvas*              _view2d;
    MyCanvas3d*            _view3d;

  private:
    void _reinitWith(Scenario*);
    void _switchViews();

  public:
    ScenarioManager();

    void setViews(MyCanvas* view2d, MyCanvas3d* view3d);

    Scenario*                     current() const;
    const std::vector<Scenario*>& scenarios() const;

  public slots:
    void reinit();
    void switchDemo(int);
};

#endif // SCENARIOMANAGER
