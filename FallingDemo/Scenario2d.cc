#include "Scenario2d.hh"

void Scenario2d::initialize()
{
  using namespace Falling2d;

  _world = new Falling2d::World2d(
      new RigidBodyGravityIntegrator2d( // FIXME: a bit clumsy
        new BodyGravityIntegrator2d(Vect2d(0.0, 9.81), Vect1d()))
      , new BroadPhase2d(0.08));

  srand(10);
}
