#include <Falling/Collision/Detection/NarrowPhase/NarrowPhase.hh>
#include "DrawCollisionGraphAccumulator.hh"

void DrawCollisionGraphAccumulator::reinit()
{ pairs.clear(); }

DrawCollisionGraphAccumulator::AccumulationResultType DrawCollisionGraphAccumulator::result()
{ return pairs; }

bool DrawCollisionGraphAccumulator::accumulate(Node<Body2d, NarrowPhase2d>& node)
{ return true; }

bool DrawCollisionGraphAccumulator::accumulate(Edge<NarrowPhase2d, Body2d>& edge)
{
  auto nf = edge.value();

  using NF  = AbstractNarrowPhase<Contact2d, Metal::ConstInheritLattice<Metal::Dynamic::Identifiable>::type>;
  using NFT = NarrowPhaseTrait<NF, Contact2d, Metal::ConstInheritLattice<Metal::Dynamic::Identifiable>::type>;

  if (NFT::numContacts(*nf)
      && !edge.pred()->value()->isStatic()
      && !edge.succ()->value()->isStatic())
    pairs.push_back(std::make_pair(edge.pred()->value(), edge.succ()->value()));

  return true;
}
