#ifndef DRAWCOLLISIONGRAPHVISITOR
# define DRAWCOLLISIONGRAPHVISITOR

# include <vector>

# include <Falling/Collision/Graph/Graph.hh>
# include <Falling2d/Aliases.hh>

using namespace Falling::Collision;
using namespace Falling2d;

class DrawCollisionGraphAccumulator
{
  public:
    using AccumulationResultType = std::vector<std::pair<const Body2d*, const Body2d*>>;

    AccumulationResultType pairs;

    void reinit();
    bool accumulate(Node<Body2d, NarrowPhase2d>& node);
    bool accumulate(Edge<NarrowPhase2d, Body2d>& edge);
    AccumulationResultType result();
};

#endif // DRAWCOLLISIONGRAPHVISITOR
