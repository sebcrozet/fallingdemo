#ifndef BALLSVEESCENARIO
# define BALLSVEESCENARIO

# include "Scenario2d.hh"
# include "plane.hh"
# include "ball.hh"

class BallsVeeScenario : public Scenario2d
{
  public:

    virtual const char* name() const
    { return "Balls Vee"; }

    virtual BallsVeeScenario* duplicateUninitialized() const
    { return new BallsVeeScenario(); }

    virtual void initialize()
    {
      using namespace Falling2d;

      srand(10);

      Scenario2d::initialize();

      _objs.push_back(new Test::Plane(*_world, Vect2d(1.0, -1.0)));
      _objs.push_back(new Test::Plane(*_world, Vect2d(-1.0, -1.0)));

      int nballs = 1500;// 1500;
      int lside  = sqrt(nballs);

      for (int i = 0; i < lside; ++i)
      {
        for (int j = 0; j < lside; ++j)
        {
          _objs.push_back(
              new Test::Ball(
                *_world
                , i * 25
                , j * 25 - 3000 // - 400
                )
              );
        }
      }
    }
};

#endif // end BALLSVEESCENARIO
