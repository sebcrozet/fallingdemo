// QIrrlichtWidget.h
 
#ifndef QIRRLICHTWIDGET_H
#define QIRRLICHTWIDGET_H
 
#include <Falling/Algebra/Vect.hh>
#include <QWidget>
#include <QGLWidget>
#include <QResizeEvent>
#include <irrlicht/irrlicht.h>
 
using namespace irr;
using namespace core;
using namespace scene;
using namespace video;
using namespace io;
using namespace Falling::Algebra;

// Our Irrlicht rendering widget
// To have everything compile, we need to run MOC on this file
class QIrrlichtWidget : public QGLWidget
{
    // Macro for the meta-object compiler MOC
    Q_OBJECT
 
public:
    explicit QIrrlichtWidget(QWidget *parent = 0);
    ~QIrrlichtWidget();
 
    // Returns a pointer to the Irrlicht Device
    IrrlichtDevice* getIrrlichtDevice();
 
    // Create the Irrlicht device and connect the signals and slots
    void init();

    virtual void hide()
    {
      killTimer(_timer);
      QGLWidget::hide();
    }

    virtual void show()
    {
      makeCurrent();
      _timer = startTimer(16);
      QGLWidget::show();
    }
 
signals:
    // Signal that its time to update the frame
    void updateIrrlichtQuery(IrrlichtDevice* device);
 
public slots:
    // Function called in response to updateIrrlichtQuery. Renders the scene in the widget
    void updateIrrlicht(IrrlichtDevice* device);
 
protected:
    virtual void paintEvent(QPaintEvent* event);
    virtual void timerEvent(QTimerEvent* event);
    virtual void resizeEvent(QResizeEvent* event);
    virtual void mouseMoveEvent(QMouseEvent* evt);
    virtual void mousePressEvent(QMouseEvent* evt);
    virtual void wheelEvent(QWheelEvent* event);
 
    IrrlichtDevice*   device;
    ICameraSceneNode* camera;

private:
    void _updateCamera();
    void _updateSize(const QSize&);

    double   _distance    = 50;
    double   _yaw         = 0.0;
    double   _pitch       = M_PI / 2.0;
    double   _yawStep     = 0.005;
    double   _pitchStep   = 0.005;
    double   _minDistance = 1.0;
    double   _maxDistance = std::numeric_limits<double>::max();
    Vect<3u> _lookAt;
    Vect<2u> _mousePressStartPoint;
    int      _timer;
};
 
#endif // QIRRWIDGET_H
