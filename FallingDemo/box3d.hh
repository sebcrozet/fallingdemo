#ifndef TEST_BOX3D
# define TEST_BOX3D

# include "SceneNode3d.hh"
# include <Falling3d/Aliases.hh>

namespace Test
{
  using namespace Falling3d;

  class Box3d : public SceneNode3d
  {
    private:
      Falling3d::Box3d* _shape;

    public:
      Box3d(World3d& world, ISceneManager* manager);
      Box3d(World3d& world, ISceneManager* manager, double x, double y, double z);

      virtual ~Box3d();
  };
} // end Test

#endif // TEST_BOX3D
