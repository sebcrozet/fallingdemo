#ifndef MYCANVAS3D
# define MYCANVAS3D

# include "ScenarioManager.hh"
# include "Scenario3d.hh"
# include "QIrrlichtWidget.hh"

class MyCanvas3d : public QIrrlichtWidget
{
  Q_OBJECT

  private:
    ScenarioManager* _manager;
    bool             _drawCollisionGraph;
    bool             _drawAABBs;
    bool             _running = false;
    bool             _step    = false;

  public:
    MyCanvas3d(ScenarioManager* manager, QWidget* parent = 0)
      : QIrrlichtWidget(parent), _manager(manager)
    { }

    ISceneManager* sceneManager() const
    { return device->getSceneManager(); }


  public slots:
    void setDrawCollisionGraph(bool draw)
    { _drawCollisionGraph = draw; }

    void setDrawAABBs(bool draw)
    { _drawAABBs = draw; }

    void startRunning()
    { _running = true; }

    void stopRunning()
    { _running = false; }

    void stepRunning()
    {
      stopRunning();
      _step = true;
    }

  signals:
    void physicsTimeChanged(double time);

    void graphicsTimeChanged(double time);

    void totalTimeChanged(double time);

  private :
    void _setPhysicsTime(double time)
    { emit physicsTimeChanged(time); }

    void _setGraphicsTime(double time)
    { emit graphicsTimeChanged(time); }

    void _setTotalTime(double time)
    { emit totalTimeChanged(time); }

    virtual void timerEvent(QTimerEvent* event)
    {
      auto timer          = sf::Clock();
      auto current        = dynamic_cast<Scenario3d*>(_manager->current());
      double physicsTime  = 0.0;
      double graphicsTime = 0.0;


      if (current->world())
      {

        timer.restart();

        if (_running || _step)
        {
          _step = false;
          current->world()->step(0.016);
        }

        physicsTime = timer.getElapsedTime().asSeconds();
        timer.restart();

        for (auto b : current->objs())
          b->draw();

        // FIXME: find a way to draw aabbs and collision graphs?

        QIrrlichtWidget::timerEvent(event);
        graphicsTime = timer.getElapsedTime().asSeconds();

        _setPhysicsTime(physicsTime);
        _setGraphicsTime(graphicsTime);
        _setTotalTime(physicsTime + graphicsTime);
      }
      else
        QIrrlichtWidget::timerEvent(event);
    }
};


#endif // MYCANVAS3D
