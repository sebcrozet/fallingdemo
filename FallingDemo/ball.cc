#include "ball.hh"
#include "Constants.hh"

namespace Test
{
  Ball::Ball(World2d& world)
    : Ball(world
           , 1000.0 * (double)rand() / (double)RAND_MAX
           , 700.0 * (double)rand() / (double)RAND_MAX)
  { }

  Ball::Ball(World2d& world, double x, double y)
  {
    auto br = 0.25 + ((double)rand() / (double)RAND_MAX) * 0.25; // radius for the rigid body
    auto r  = br * DRAW_SCALE; // radius for the display
    auto cr = ((double)rand() / (double)RAND_MAX) * 100;
    auto cg = ((double)rand() / (double)RAND_MAX) * 100;
    auto cb = ((double)rand() / (double)RAND_MAX) * 100;

    _image.setRadius(r - THICKNESS);
    _image.setFillColor(sf::Color(cr, cg, cb));
    _image.setOrigin(r, r);
    _image.setOutlineThickness(THICKNESS);
    _image.setOutlineColor(sf::Color(cr + 100, cg + 100, cb + 100));

    Vect2d trans(x / DRAW_SCALE, y / DRAW_SCALE);

    _shape = new Ball2d(br);
    _body = new Body2d(_shape, true);
    translate({ x / DRAW_SCALE, y / DRAW_SCALE }, *_body);
    world.addBody(_body);
  }

  void Ball::draw(sf::RenderWindow& rw)
  {
    auto pos = translation(*_body);
    _image.setPosition((float)pos.at(0) * DRAW_SCALE,
                       (float)pos.at(1) * DRAW_SCALE);
    rw.draw(_image);
  }

  Body2d* Ball::physics()
  {
    return _body;
  }

  Ball::~Ball()
  {
    delete _body;
    delete _shape;
  }
} // end Test
