#ifndef TEST_BALL
# define TEST_BALL

# include "Sfml.hh"
# include "SceneNode.hh"
# include <Falling2d/Aliases.hh>

namespace Test
{
  using namespace Falling2d;

  class Ball : public SceneNode
  {
    private:
    Body2d*         _body;
    Ball2d*         _shape;
    sf::CircleShape _image;

    public:
    Ball(World2d& world);
    Ball(World2d& world, double x, double y);
    virtual void draw(sf::RenderWindow& rw);
    virtual Body2d* physics();

    virtual ~Ball();
  };
} // end Test

#endif // TEST_BALL
