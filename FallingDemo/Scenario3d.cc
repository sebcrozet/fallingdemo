#include "Scenario3d.hh"

void Scenario3d::initialize()
{
  using namespace Falling3d;

  _world = new World3d(
      new RigidBodyGravityIntegrator3d( // FIXME: a bit clumsy
        new BodyGravityIntegrator3d(Vect3d(0.0, -9.81, 0.0), Vect3d()))
      , new BroadPhase3d(0.08));

  srand(10);

  // add lights to the scene
  _sceneManager->setAmbientLight(video::SColorf(0.75, 0.75, 0.75, 1.0));

  auto light = _sceneManager->addLightSceneNode(
      0
      , core::vector3df(-200, 400, -200)
      , video::SColorf(0.3f, 0.3f, 0.3f)
      , 1.0f
      , 1);
  SLight& l = light->getLightData();
  l.Type = ELT_DIRECTIONAL;

}
