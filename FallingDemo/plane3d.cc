#include "plane3d.hh"

namespace Test
{
  Plane3d::Plane3d(World3d& world, Vect3d n)
    : SceneNode3d(nullptr)
  {
    auto normal = normalized(n);
    auto pos    = Vect3d();

    _plane   = new Falling3d::Plane3d(normal, pos);
    _physics = new Body3d(_plane);

    world.addBody(_physics);
  }

  Plane3d::~Plane3d()
  {
    delete _plane;
  }
} // end Test
