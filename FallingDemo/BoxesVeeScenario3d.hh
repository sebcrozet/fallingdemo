#ifndef BOXESSVEESCENARIO3D
# define BOXESSVEESCENARIO3D

# include "Scenario3d.hh"
# include "plane3d.hh"
# include "box3d.hh"

class BoxesVeeScenario3d : public Scenario3d
{
  public:
    virtual const char* name() const
    { return "Boxes Vee 3d"; }

    virtual BoxesVeeScenario3d* duplicateUninitialized() const
    {
      return new BoxesVeeScenario3d(_sceneManager);
    }

    BoxesVeeScenario3d(ISceneManager* manager)
      : Scenario3d(manager)
    { }

    virtual void initialize()
    {
      using namespace Falling3d;

      srand(10);

      Scenario3d::initialize();

      _objs.push_back(new Test::Plane3d(*_world, Vect3d(-1.0, 1.0, -1.0)));
      _objs.push_back(new Test::Plane3d(*_world, Vect3d(1.0, 1.0, 1.0)));
      _objs.push_back(new Test::Plane3d(*_world, Vect3d(-1.0, 1.0, 1.0)));
      _objs.push_back(new Test::Plane3d(*_world, Vect3d(1.0, 1.0, -1.0)));

      int lside  = 10;

      for (int i = 0; i < lside; ++i)
      {
        for (int j = 0; j < lside; ++j)
        {
          for (int k = 0; k < lside; ++k)
          {
            _objs.push_back(
                new Test::Box3d(
                  *_world
                  , _sceneManager
                  , i * 2
                  , j * 2 + 10
                  , k * 2
                  )
                );
          }
        }
      }
    }
};

#endif // end BOXESSVEESCENARIO3D
