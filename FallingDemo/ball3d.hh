#ifndef TEST_BALL3D
# define TEST_BALL3D

# include "SceneNode3d.hh"
# include <Falling3d/Aliases.hh>

namespace Test
{
  using namespace Falling3d;

  class Ball3d : public SceneNode3d
  {
    private:
      Falling3d::Ball3d* _shape;

    public:
      Ball3d(World3d& world, ISceneManager* manager);
      Ball3d(World3d& world, ISceneManager* manager, double x, double y, double z);

      virtual ~Ball3d();
  };
} // end Test


#endif // TEST_BALL3D
