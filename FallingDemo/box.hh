#ifndef TEST_BOX
# define TEST_BOX

# include "Sfml.hh"
# include "SceneNode.hh"
# include <Falling2d/Aliases.hh>

namespace Test
{
  class Box : public SceneNode
  {
    private:
      Falling2d::Body2d* _body;
      Falling2d::Box2d*  _shape;
      sf::RectangleShape _image;

    public:
    Box(Falling2d::World2d& world);
    Box(Falling2d::World2d& world, double x, double y);
    virtual void draw(sf::RenderWindow& rw);
    virtual Falling2d::Body2d* physics();

    virtual ~Box();
  };
} // end Test

#endif // TEST_BOX
