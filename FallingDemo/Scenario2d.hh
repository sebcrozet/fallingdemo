#ifndef SCENARIO2D
# define SCENARIO2D

# include <Falling2d/Aliases.hh>
# include "SceneNode.hh"
# include "Scenario.hh"

class Scenario2d : public Scenario
{
  protected:
    Falling2d::World2d*           _world;
    std::vector<Test::SceneNode*> _objs;

  public:
    virtual void        initialize();
    virtual Scenario*   duplicateUninitialized() const = 0;
    virtual const char* name() const = 0;

    virtual bool is2d() const
    { return true; }

    Falling2d::World2d* world()
    { return _world; }

    const std::vector<Test::SceneNode*>& objs() const
    { return _objs; }

    virtual ~Scenario2d()
    {
      delete _world;

      for (auto obj : _objs)
        delete obj;
    }
};

#endif // end SCENARIO2D
