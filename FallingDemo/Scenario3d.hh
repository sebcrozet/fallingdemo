#ifndef SCENARIO3D
# define SCENARIO3D

# include <irrlicht/irrlicht.h>
# include <Falling3d/Aliases.hh>
# include "SceneNode3d.hh"
# include "Scenario.hh"

using namespace irr;
using namespace core;
using namespace scene;
using namespace video;
using namespace io;
using namespace Falling::Algebra;

class Scenario3d : public Scenario
{
  protected:
    Falling3d::World3d*       _world;
    std::vector<SceneNode3d*> _objs;
    ISceneManager*            _sceneManager;

  public:
    virtual void        initialize();
    virtual Scenario*   duplicateUninitialized() const = 0;
    virtual const char* name() const = 0;

    virtual bool is2d() const
    { return false; }

    Scenario3d(ISceneManager* manager)
    { _sceneManager = manager; }

    Falling3d::World3d* world()
    { return _world; }

    const std::vector<SceneNode3d*>& objs() const
    { return _objs; }

    virtual ~Scenario3d()
    {
      delete _world;

      for (auto obj : _objs)
        delete obj;

      _sceneManager->clear();
    }
};

#endif // end SCENARIO3D
