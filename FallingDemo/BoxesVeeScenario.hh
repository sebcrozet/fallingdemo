#ifndef BOXSVEESCENARIO
# define BOXSVEESCENARIO

# include "Scenario2d.hh"
# include "plane.hh"
# include "box.hh"

class BoxesVeeScenario : public Scenario2d
{
  public:

    virtual const char* name() const
    { return "Boxes Vee"; }

    virtual BoxesVeeScenario* duplicateUninitialized() const
    { return new BoxesVeeScenario(); }

    virtual void initialize()
    {
      using namespace Falling2d;

      srand(10);

      Scenario2d::initialize();

      _objs.push_back(new Test::Plane(*_world, Vect2d(1.0, -1.0)));
      _objs.push_back(new Test::Plane(*_world, Vect2d(-1.0, -1.0)));

      int nboxes = 1500; // 1500;
      int lside  = sqrt(nboxes);

      for (int i = 0; i < lside; ++i)
      {
        for (int j = 0; j < lside; ++j)
        {
          _objs.push_back(
              new Test::Box(
                *_world
                , i * 25
                , j * 25 // - 3000 // - 400
                )
              );
        }
      }
    }
};

#endif // end BOXSVEESCENARIO
