#include "QSFMLCanvas.hh"
#include <qapplication.h>
#include <qframe.h>
#include <QHBoxLayout>
#include <QPushButton>
#include <QCheckBox>
#include <QComboBox>
#include <QLabel>

#include <Falling2d/Aliases.hh>
#include <Falling2d/Matrix2d.hh>
#include <Falling2d/Vector2d.hh>

#include "MyCanvas.hh"
#include "MyCanvas3d.hh"
#include "Sfml.hh"
#include "Constants.hh"
#include "ball.hh"
#include "plane.hh"
#include "DrawCollisionGraphAccumulator.hh"
#include "ScenarioManager.hh"
#include "QIrrlichtWidget.hh"

void initRunStopStep(MyCanvas*          view
                     , MyCanvas3d*      view3d
                     , QLayout*         layout
                     , ScenarioManager* scenarios)
{
  QPushButton* run     = new QPushButton("Run");
  QPushButton* stop    = new QPushButton("Pause");
  QPushButton* step    = new QPushButton("Step");
  QPushButton* restart = new QPushButton("Restart");

  QObject::connect(run,     SIGNAL(clicked(bool)), view,      SLOT(startRunning()));
  QObject::connect(stop,    SIGNAL(clicked(bool)), view,      SLOT(stopRunning()));
  QObject::connect(step,    SIGNAL(clicked(bool)), view,      SLOT(stepRunning()));
  QObject::connect(run,     SIGNAL(clicked(bool)), view3d,    SLOT(startRunning()));
  QObject::connect(stop,    SIGNAL(clicked(bool)), view3d,    SLOT(stopRunning()));
  QObject::connect(step,    SIGNAL(clicked(bool)), view3d,    SLOT(stepRunning()));
  QObject::connect(restart, SIGNAL(clicked(bool)), scenarios, SLOT(reinit()));

  run->setMaximumWidth(150);
  stop->setMaximumWidth(150);
  step->setMaximumWidth(150);
  restart->setMaximumWidth(150);

  layout->addWidget(run);
  layout->addWidget(stop);
  layout->addWidget(step);
  layout->addWidget(restart);
}

void initDrawOptions(MyCanvas* view, MyCanvas3d* view3d, QLayout* layout)
{
  QCheckBox* drawGraph = new QCheckBox("Collision graph");
  QCheckBox* drawAABBs = new QCheckBox("Bounding volumes");

  QObject::connect(drawGraph, SIGNAL(toggled(bool)), view, SLOT(setDrawCollisionGraph(bool)));
  QObject::connect(drawAABBs, SIGNAL(toggled(bool)), view, SLOT(setDrawAABBs(bool)));
  QObject::connect(drawGraph, SIGNAL(toggled(bool)), view3d, SLOT(setDrawCollisionGraph(bool)));
  QObject::connect(drawAABBs, SIGNAL(toggled(bool)), view3d, SLOT(setDrawAABBs(bool)));

  drawGraph->setMaximumWidth(150);
  drawAABBs->setMaximumWidth(150);

  layout->addWidget(drawGraph);
  layout->addWidget(drawAABBs);
}

void initScenariosPickList(QLayout* layout, ScenarioManager* scenarios)
{
  QComboBox* list = new QComboBox();

  for (auto scenario : scenarios->scenarios())
    list->addItem(scenario->name());

  list->setMaximumWidth(150);

  QObject::connect(list, SIGNAL(currentIndexChanged(int)), scenarios, SLOT(switchDemo(int)));

  layout->addWidget(list);
}

void initTimeIndicators(MyCanvas* view, MyCanvas3d* view3d, QLayout* layout)
{
  // FIXME: find a way to merge labels and time values
  QLabel* physicsTimeT  = new QLabel("Physics:");
  QLabel* physicsTime   = new QLabel("00000");
  QLabel* graphicsTimeT = new QLabel("Graphics:");
  QLabel* graphicsTime  = new QLabel("00000");
  QLabel* totalTimeT    = new QLabel("Total:");
  QLabel* totalTime     = new QLabel("00000");

  physicsTime->setMaximumWidth(150);
  graphicsTime->setMaximumWidth(150);
  totalTime->setMaximumWidth(150);
  physicsTimeT->setMaximumWidth(150);
  graphicsTimeT->setMaximumWidth(150);
  totalTimeT->setMaximumWidth(150);

  physicsTime->setMaximumHeight(20);
  graphicsTime->setMaximumHeight(20);
  totalTime->setMaximumHeight(20);
  physicsTimeT->setMaximumHeight(20);
  graphicsTimeT->setMaximumHeight(20);
  totalTimeT->setMaximumHeight(20);

  // FIXME: setup signals

  layout->addWidget(physicsTimeT);
  layout->addWidget(physicsTime);
  layout->addWidget(graphicsTimeT);
  layout->addWidget(graphicsTime);
  layout->addWidget(totalTimeT);
  layout->addWidget(totalTime);

  QObject::connect(view,   SIGNAL(physicsTimeChanged(double)),  physicsTime,  SLOT(setNum(double)));
  QObject::connect(view,   SIGNAL(graphicsTimeChanged(double)), graphicsTime, SLOT(setNum(double)));
  QObject::connect(view,   SIGNAL(totalTimeChanged(double)),    totalTime,    SLOT(setNum(double)));
  QObject::connect(view3d, SIGNAL(physicsTimeChanged(double)),  physicsTime,  SLOT(setNum(double)));
  QObject::connect(view3d, SIGNAL(graphicsTimeChanged(double)), graphicsTime, SLOT(setNum(double)));
  QObject::connect(view3d, SIGNAL(totalTimeChanged(double)),    totalTime,    SLOT(setNum(double)));
}

void initUI()
{
#if 0
  // test the GJK
  std::cout << " distance: " <<
    GJK::distanceBetween<JohnsonSimplex<Vect2d>, Ball2d, Ball2d, Vect2d, Error<double>>
    (Ball2d(1.0, Vect2d(10.0, 0.0)), Ball2d(2.0))
    << std::endl;

  std::cout << std::endl << std::endl << "test 2" << std::endl;
  std::cout << " distance: " <<
    GJK::distanceBetween<JohnsonSimplex<Vect2d>, Ball2d, Box2d, Vect2d, Error<double>>
    (Ball2d(1.0, Vect2d(4.0, 3.0)), Box2d(Vect2d(10.5, 1.0)))
    << std::endl;

  std::cout << std::endl << std::endl << "test 2" << std::endl;
  std::pair<Vect2d, Vect2d> pts;
  GJK::closestPoints<JohnsonSimplex<AnnotatedSupportPoint<Vect2d>>, Ball2d, Box2d, Vect2d, Error<double>>
    (Ball2d(1.0, Vect2d(4.0, 3.0)), Box2d(Vect2d(1.0, 1.0)), pts);
  std::cout << "Closest points: " << std::get<0>(pts) << " " << std::get<1>(pts) << std::endl;
#endif

  // reproduce buggy cituation:
  Transform2d a = MultInversibleSemiGroupTrait<Transform2d>::one;
  translate({29.282295683797585, 33.502252656878831}, a);

  // auto sa = Shape::Static::transformShape(Box2d({0.39145268308532083, 0.40273247945715324}), a, nullptr);
  
  // Transform2d b = MultInversibleSemiGroupTrait<Transform2d>::one;
  // translate({29.945240600301894, 34.267351581892811}, b);
  // auto sb = Shape::Static::transformShape(Box2d({0.29586791354039121, 0.39616324049241991}), b, nullptr);

  // translate({-29.945240600301894, -34.267351581892811}, a);
  // auto cso = Shape::Static::transformShape(
  //     Box2d({0.39145268308532083 + 0.29586791354039121, 0.40273247945715324 + 0.39616324049241991})
  //     , a
  //     , nullptr);

  // std::cout << a << " " <<0.39145268308532083 + 0.29586791354039121 << " " << 0.40273247945715324 + 0.39616324049241991 << std::endl;

  // using Box2dT = TransformedShape<Box2d, Transform2d>;
  // MinkowskiSampling::projectOrigin<JohnsonSimplex<Vect2d>, Box2dT, Vect2d, Error<double>, 25>
  //   (*cso);
  //
  JohnsonSimplex<Vect2d> splx;
  splx.addPoint(Vect2d({0.16545851061677860349696800312813138589262962341308594, -1.381388270962667252916844518040306866168975830078125}));
  std::cout << splx << std::endl;
  splx.addPoint(Vect2d({0.16545851061677860349696800312813138589262962341308594, 0.21640316893647290608093669561640126630663871765136719}));
  std::cout << splx << std::endl;
  splx.addPoint(Vect2d({0.16545851061677860349696800312813138589262962341308594, -0.589061789977826766318003137712366878986358642578125}));
  std::cout << splx << std::endl;
  auto proj = splx.projectOrigin();
  std::cout << "PROJECTION: " << proj << std::endl;
  std::cout << splx << std::endl;

  std::cout << "PROJECTION Dimension: " << splx.dimension() << std::endl;

  // MinkowskiSampling::distanceBetween<JohnsonSimplex<Vect2d>, Box2dT, Box2dT, Vect2d, Error<double>, 25>
  //   (*sa, *sb);

  // Create the main frame
  QFrame* mainFrame = new QFrame;
  mainFrame->setWindowTitle("Qt SFML");
  mainFrame->resize(400, 400);
  mainFrame->show();

  QHBoxLayout* hboxLayout = new QHBoxLayout(mainFrame);
  QVBoxLayout* vboxLayout = new QVBoxLayout();


  auto scenarioManager = new ScenarioManager();

  // Create a SFML view inside the main frame
  MyCanvas*   SFMLView = new MyCanvas(mainFrame, QPoint(20, 20), QSize(360, 360), scenarioManager);
  std::cout << "Will create ogre view." << std::endl;
  MyCanvas3d* irrlichtView = new MyCanvas3d(scenarioManager, mainFrame);
  std::cout << "Created ogre view." << std::endl;
  irrlichtView->init();
  hboxLayout->addWidget(SFMLView);
  hboxLayout->addWidget(irrlichtView);
  irrlichtView->hide();
  SFMLView->hide();

  scenarioManager->setViews(SFMLView, irrlichtView);

  hboxLayout->addLayout(vboxLayout);

  initScenariosPickList(vboxLayout, scenarioManager);
  initRunStopStep(SFMLView, irrlichtView, vboxLayout, scenarioManager);
  initDrawOptions(SFMLView, irrlichtView, vboxLayout);
  initTimeIndicators(SFMLView, irrlichtView, vboxLayout);

  vboxLayout->setSizeConstraint(QLayout::SetMinimumSize);

}

int main(int argc, char** argv)
{
  QApplication app(argc, argv);

  initUI();

  return app.exec();
}
