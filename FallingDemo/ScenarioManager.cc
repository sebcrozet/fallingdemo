#include "ScenarioManager.hh"
#include "BallsVeeScenario.hh"
#include "BallsVeeScenario3d.hh"
#include "BoxesVeeScenario.hh"
#include "BoxesVeeScenario3d.hh"
#include "MyCanvas.hh"
#include "MyCanvas3d.hh"

ScenarioManager::ScenarioManager()
{ }

void ScenarioManager::_switchViews()
{
  if (_current->is2d())
  {
    _view3d->hide();
    _view2d->show();
  }
  else
  {
    _view2d->hide();
    _view3d->show();
  }
}

void ScenarioManager::setViews(MyCanvas* view2d, MyCanvas3d* view3d)
{
  _view2d = view2d;
  _view3d = view3d;

  _all.push_back(new BallsVeeScenario());
  _all.push_back(new BoxesVeeScenario());
  _all.push_back(new BallsVeeScenario3d(view3d->sceneManager()));
  _all.push_back(new BoxesVeeScenario3d(view3d->sceneManager()));

  _current = _all[0]->duplicateUninitialized();

  _switchViews(); // must be called before the initialize (to ensure 3D is initialized before)

  _current->initialize();
}

Scenario* ScenarioManager::current() const
{ return _current; }

const std::vector<Scenario*>& ScenarioManager::scenarios() const
{ return _all; }

void ScenarioManager::reinit()
{
  _reinitWith(_current);
}

void ScenarioManager::_reinitWith(Scenario* newOne)
{
  auto oldScenario = _current;

  _current = newOne->duplicateUninitialized();

  delete oldScenario;

  _switchViews(); // must be called before the initialize (to ensure 3D is initialized before)

  _current->initialize();
}

void ScenarioManager::switchDemo(int id)
{
  _reinitWith(_all[id]);
}
