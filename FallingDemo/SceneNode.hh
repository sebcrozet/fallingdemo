#ifndef TEST_SCENENODE
# define TEST_SCENENODE

# include "Sfml.hh"
# include <Falling2d/Aliases.hh>

namespace Test
{
  class SceneNode
  {
    public:
      virtual void draw(sf::RenderWindow&) = 0;
      virtual Falling2d::Body2d* physics() = 0;
      virtual ~SceneNode()
      { }
  };
} // end Test


#endif // TEST_SCENENODE
