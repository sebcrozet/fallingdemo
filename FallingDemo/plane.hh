#ifndef TEST_PLANE
# define TEST_PLANE

# include <Falling2d/Aliases.hh>
# include "SceneNode.hh"

# include "Sfml.hh"

namespace Test
{
  using namespace Falling2d;

  class Plane : public SceneNode
  {
    private:
      Body2d*         _body;
      Plane2d*        _plane;
      sf::ConvexShape _shape;
    public:
      Plane(World2d& world, Vect2d normal);

      virtual void draw(sf::RenderWindow&);
      virtual Body2d* physics();
      virtual ~Plane();
  };
} // end Test

#endif // TEST_PLANE
