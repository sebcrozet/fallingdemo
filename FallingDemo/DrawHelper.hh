#ifndef DRAWHELPER
# define DRAWHELPER

# include <Falling2d/Aliases.hh>
# include "Sfml.hh"

struct DrawHelper
{
    static void drawLine (float             xa,
                          float             ya,
                          float             xb,
                          float             yb,
                          const sf::Color&  c,
                          sf::RenderWindow& rwin)
    {
      sf::Vertex vertices[2];
      vertices[0] = sf::Vertex (sf::Vector2f(xa, ya), c);
      vertices[1] = sf::Vertex (sf::Vector2f(xb, yb), c);
      rwin.draw (vertices, 2, sf::LinesStrip);
    }

    static void drawCollisionGraph(World2d& w, sf::RenderWindow& rw)
    {
      auto cg = w.collisionGraph();

      auto bodies  = w.bodies();
      auto islands = cg->accumulate<DrawCollisionGraphAccumulator>(bodies);

      for (auto island : islands)
      {
        for (auto pairs : island)
        {
          auto pos1 = translation(localToWorld(*std::get<0>(pairs)));
          auto pos2 = translation(localToWorld(*std::get<1>(pairs)));

          drawLine(pos1.components[0] * DRAW_SCALE
                   , pos1.components[1] * DRAW_SCALE
                   , pos2.components[0] * DRAW_SCALE
                   , pos2.components[1] * DRAW_SCALE
                   , sf::Color::White
                   , rw);
        }
      }
    }

    static void drawBoundingVolumes(sf::RenderWindow&                      rw
                                    , const std::vector<Test::SceneNode*>& objs)
    {
      for (auto obj : objs)
      {
        auto body = obj->physics();
        auto bb   = body->proxy<BVHTreeNodeProxy<BoundingVolume2d>>().treeNode<Body2d>()->boundingVolume;
        drawLine(
            bb.mins[0] * DRAW_SCALE
            , bb.mins[1] * DRAW_SCALE
            , bb.maxs[0] * DRAW_SCALE
            , bb.mins[1] * DRAW_SCALE
            , sf::Color::White
            , rw);
        drawLine(
            bb.maxs[0] * DRAW_SCALE
            , bb.maxs[1] * DRAW_SCALE
            , bb.maxs[0] * DRAW_SCALE
            , bb.mins[1] * DRAW_SCALE
            , sf::Color (255, 255, 255)
            , rw);
        drawLine(
            bb.mins[0] * DRAW_SCALE
            , bb.mins[1] * DRAW_SCALE
            , bb.mins[0] * DRAW_SCALE
            , bb.maxs[1] * DRAW_SCALE
            , sf::Color (255, 255, 255)
            , rw);
        drawLine(
            bb.maxs[0] * DRAW_SCALE
            , bb.maxs[1] * DRAW_SCALE
            , bb.mins[0] * DRAW_SCALE
            , bb.maxs[1] * DRAW_SCALE
            , sf::Color (255, 255, 255)
            , rw);
      }
    }
};

#endif // DRAWHELPER
