#ifndef SCENARIO
# define SCENARIO

# include "SceneNode.hh"

class Scenario
{
  public:
    virtual void initialize()                          = 0;


    virtual bool        is2d()                   const = 0;
    virtual Scenario*   duplicateUninitialized() const = 0;
    virtual const char* name()                   const = 0;

    virtual ~Scenario()
    { }
};

#endif // end SCENARIO
