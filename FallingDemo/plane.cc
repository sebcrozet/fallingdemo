#include "plane.hh"

namespace Test
{
  Plane::Plane(World2d& world, Vect2d n)
  {
    auto normal = normalized(n);
    auto pos    = Vect2d(10.0, -10.0);

    _plane = new Plane2d(normal, pos);
    _body  = new Body2d(_plane);
    translate(Vect2d(20.0, 45), *_body);

    world.addBody(_body);
  }

  Plane::~Plane()
  {
    delete _body;
    delete _plane;
  }

  Body2d* Plane::physics()
  {
    return _body;
  }

  void Plane::draw(sf::RenderWindow& rw)
  {
    return;

    using namespace Falling::Algebra;
    using namespace Falling::Shape;

    // build a list of all points on the screen
    // and insert the first point at the end of
    // the list also
    Vect2d       screenpts[5];
    sf::Vector2f cvpt;
    Vect2d       inter1;
    Vect2d       inter2;

    screenpts[0] = Vect2d(0             , 0);
    screenpts[1] = Vect2d(rw.getSize().x, 0);
    screenpts[2] = Vect2d(rw.getSize().x, rw.getSize().y);
    screenpts[3] = Vect2d(0             , rw.getSize().y);
    screenpts[4] = screenpts[0];

    int transition = 0;
    int inters     = 0;

    std::vector<Vect2d> pts;

    for (unsigned i = 0; i < 5; i++)
    {
      // FIXME: const Plane* plane = static_cast<Plane*>(_plane->shape());

      if (false) // FIXME: dot(plane->normal(), sub(plane->center(), screenpts[i])) > 0) // FIXME rb->containsPoint(screenpts[i]))
      {
        if (transition == -1)
        {
          // transition not contained -> contained
          // determine the plane's intersection with the corresponding segment
          if (false) // Vect2d::intersectLines(screenpts[i - 1], screenpts[i], diskcenter + u, diskcenter, &inter1))
          {
            // add the point on the list
            pts.push_back(inter1);
            inters++;
          }
        }

        pts.push_back(screenpts[i]);
        //add the point
        transition = 1;
      }
      else
      {
        // point not contained
        if (transition == 1)
        {
          // transition contained -> not contained
          // determine the plane's intersection with the corresponding segment
          if (false) // FIXME Vect2d::intersectLines(screenpts[i - 1], screenpts[i], diskcenter + u, diskcenter, &inter2))
          {
            // add the point on the list
            pts.push_back(inter2);
            inters++;
          }
        }
        transition = -1;
      }
    }

    for (int i = 0; i < pts.size(); ++i) {
      _shape.setPoint(i, sf::Vector2f(pts[i].components[0], pts[i].components[1]));
    }

    rw.draw(_shape);

    if (inters == 2)
    {
      sf::Vertex vertices[2];
      sf::Color  c(200, 200, 200);

      vertices[0] = sf::Vertex(sf::Vector2f(inter1.components[0], inter1.components[1]), c);
      vertices[1] = sf::Vertex(sf::Vector2f(inter2.components[0], inter2.components[1]), c);

      rw.draw(vertices, 2, sf::LinesStrip);
    }
  }
} // end Test
