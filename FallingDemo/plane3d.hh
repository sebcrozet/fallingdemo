#ifndef TEST_PLANE3D
# define TEST_PLANE3D

# include <Falling3d/Aliases.hh>
# include "SceneNode3d.hh"

namespace Test
{
  using namespace Falling3d;

  class Plane3d : public SceneNode3d
  {
    private:
      Falling3d::Plane3d* _plane;

    public:
      Plane3d(World3d& world, Vect3d n);

      virtual ~Plane3d();
  };
}

#endif // TEST_PLANE3D
