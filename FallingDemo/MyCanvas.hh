#ifndef MYCANVAS
# define MYCANVAS

#include <Falling2d/Aliases.hh>
#include <Falling2d/Matrix2d.hh>
#include <Falling2d/Vector2d.hh>

#include "Sfml.hh"
#include "Constants.hh"
#include "ball.hh"
#include "plane.hh"
#include "DrawCollisionGraphAccumulator.hh"
#include "QSFMLCanvas.hh"
#include "ScenarioManager.hh"
#include "Scenario2d.hh"
#include "DrawHelper.hh"

////////////////////////////////////////////////////////////
/// Custom SFML canvas
////////////////////////////////////////////////////////////
class MyCanvas : public QSFMLCanvas
{
  Q_OBJECT

  private:
    ScenarioManager* _manager;
    bool             _drawAABBs;
    bool             _drawCollisionGraph;
    bool             _running = false;
    bool             _step    = false;

  public :
    ////////////////////////////////////////////////////////////
    /// Construct the canvas
    ///
    ////////////////////////////////////////////////////////////
    MyCanvas(QWidget* Parent, const QPoint& Position, const QSize& Size, ScenarioManager* manager)
      : QSFMLCanvas(Parent, Position, Size), _manager(manager)
        , _drawAABBs(false), _drawCollisionGraph(false)
    {
    }

    public slots:
    void setDrawCollisionGraph(bool draw)
    { _drawCollisionGraph = draw; }

    void setDrawAABBs(bool draw)
    { _drawAABBs = draw; }

    void startRunning()
    { _running = true; }

    void stopRunning()
    { _running = false; }

    void stepRunning()
    {
      stopRunning();
      _step = true;
    }

  signals:
    void physicsTimeChanged(double time);

    void graphicsTimeChanged(double time);

    void totalTimeChanged(double time);

  private :
    void _setPhysicsTime(double time)
    { emit physicsTimeChanged(time); }

    void _setGraphicsTime(double time)
    { emit graphicsTimeChanged(time); }

    void _setTotalTime(double time)
    { emit totalTimeChanged(time); }

    void OnInit()
    {
      // Load the image
    }

    void OnUpdate()
    {
      auto timer   = sf::Clock();
      auto current = dynamic_cast<Scenario2d*>(_manager->current());

      timer.restart();

      if (_running || _step)
      {
        _step = false;
        current->world()->step(0.016);
      }

      double physicsTime = timer.getElapsedTime().asSeconds();
      timer.restart();

      clear(sf::Color(0, 0, 0));

      for (auto b : current->objs())
        b->draw(*this);

      if (_drawAABBs)
        DrawHelper::drawBoundingVolumes(*this, current->objs());

      if (_drawCollisionGraph)
        DrawHelper::drawCollisionGraph(*current->world(), *this);

      double graphicsTime = timer.getElapsedTime().asSeconds();

      _setPhysicsTime(physicsTime);
      _setGraphicsTime(graphicsTime);
      _setTotalTime(physicsTime + graphicsTime);
    }
};

#endif // MYCANVAS
