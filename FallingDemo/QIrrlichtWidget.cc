
// QIrrlichtWidget.cpp

# include "QIrrlichtWidget.hh"
# include <iostream>
# include <QtCore/QDebug>

QIrrlichtWidget::QIrrlichtWidget(QWidget *parent) :
    QGLWidget(parent)
{
    // Indicates that the widget wants to draw directly onto the screen. (From documentation :
    // http://doc.qt.nokia.com/latest/qt.html)
    // Essential to have this or there will be nothing displayed
    setAttribute(Qt::WA_PaintOnScreen, true);
    setAttribute(Qt::WA_NoBackground);
    setAttribute(Qt::WA_NoSystemBackground);
    // Indicates that the widget paints all its pixels when it receives a paint event.
    // Thus, it is not required for operations like updating, resizing, scrolling and focus changes
    // to erase the widget before generating paint events.
    // Not sure this is required for the program to run properly, but it is here just incase.
    setAttribute(Qt::WA_OpaquePaintEvent);
    // Widget accepts focus by both tabbing and clicking
    setFocusPolicy(Qt::StrongFocus);
    // Not sure if this is necessary, but it was in the code I am basing this solution off of
    setAutoFillBackground(false);

    device = 0;
}

QIrrlichtWidget::~QIrrlichtWidget()
{
    if(device != 0)
    {
        device->closeDevice();
        device->drop();
    }
}

// Create the Irrlicht device and connect the signals and slots
void QIrrlichtWidget::init()
{
    // Make sure we can't create the device twice
    if(device != 0)
        return;

    // Set all the device creation parameters
    SIrrlichtCreationParameters params;
    params.AntiAlias        = 2;
    params.Bits             = 32;
    params.DeviceType       = EIDT_X11;
    params.Doublebuffer     = true;
    params.DriverType       = EDT_OPENGL;
    params.EventReceiver    = 0;
    params.Fullscreen       = false;
    params.HighPrecisionFPU = false;
    params.IgnoreInput      = false;
    params.LoggingLevel     = ELL_INFORMATION;
    params.Stencilbuffer    = true;
    params.Stereobuffer     = false;
    params.Vsync            = false;
    // Specify which window/widget to render to
    params.WindowId          = reinterpret_cast<void*>(winId());
    params.WindowSize.Width  = width();
    params.WindowSize.Height = height();
    params.WithAlphaChannel  = false;
    params.ZBufferBits       = 16;

    // Create the Irrlicht Device with the previously specified parameters
    device = createDeviceEx(params);

    if(device)
    {
        // Create a camera so we can view the scene
        _updateCamera();

        // auto sphere = device->getSceneManager()->addSphereSceneNode();
        // sphere->setMaterialFlag(EMF_LIGHTING, true);
        // sphere->getMaterial(0).Shininess = 20.0f;
        // sphere->getMaterial(0).SpecularColor.set(255,255,255,255);
        // sphere->getMaterial(0).AmbientColor.set(255,255,255,255); 
        // sphere->getMaterial(0).DiffuseColor.set(255,255,255,255); 
        // sphere->getMaterial(0).EmissiveColor.set(0,0,0,0);

        device->getSceneManager()->setAmbientLight(video::SColorf(0.3, 0.3, 0.3, 1));

        auto light = device->getSceneManager()->addLightSceneNode(
            0
            , core::vector3df(0, 400, -200)
            , video::SColorf(0.3f, 0.3f, 0.3f)
            , 1.0f
            , 1 );
        SLight& l = light->getLightData();
        l.Type = ELT_DIRECTIONAL;

    }

    // Connect the update signal (updateIrrlichtQuery) to the update slot (updateIrrlicht)
    connect(this
            , SIGNAL(updateIrrlichtQuery(IrrlichtDevice*))
            , this
            , SLOT(updateIrrlicht(IrrlichtDevice*)));

    makeCurrent();
}

IrrlichtDevice* QIrrlichtWidget::getIrrlichtDevice()
{
    return device;
}

void QIrrlichtWidget::paintEvent(QPaintEvent* event)
{
  if(device != 0)
  {
    emit updateIrrlichtQuery(device);
  }
}

void QIrrlichtWidget::timerEvent(QTimerEvent* event)
{
  // Emit the render signal each time the timer goes off
  if (device != 0)
  {
    emit updateIrrlichtQuery(device);
  }

  event->accept();
  _updateSize(size());
}

void QIrrlichtWidget::resizeEvent(QResizeEvent* event)
{
  _updateSize(event->size());

  QWidget::resizeEvent(event);
}

void QIrrlichtWidget::_updateSize(const QSize& size)
{
    if(device != 0)
    {
        dimension2d<u32> widgetSize;
        widgetSize.Width  = size.width();
        widgetSize.Height = size.height();
        device->getVideoDriver()->OnResize(widgetSize);

        ICameraSceneNode *cam = device->getSceneManager()->getActiveCamera();

        if (cam != 0)
          cam->setAspectRatio((f32)widgetSize.Width / (f32)widgetSize.Height);
    }
}

void QIrrlichtWidget::updateIrrlicht(irr::IrrlichtDevice* device)
{
    if(device != 0)
    {
        device->getTimer()->tick();

        SColor color(255, 0 , 0, 0);

        device->getVideoDriver()->beginScene(true, true, color);
        device->getSceneManager()->drawAll();
        device->getVideoDriver()->endScene();
    }
}

void QIrrlichtWidget::mousePressEvent(QMouseEvent* evt)
{
  _mousePressStartPoint = Vect<2u>(evt->pos().x(), evt->pos().y());
}

void QIrrlichtWidget::mouseMoveEvent(QMouseEvent* evt)
{
  Vect<2u> mouseMovePoint(evt->x(), evt->y());

  Vect<2u> rel = mouseMovePoint - _mousePressStartPoint;

  _yaw   += -rel[0] * _yawStep;
  _pitch += -rel[1] * _pitchStep;

  _mousePressStartPoint = mouseMovePoint;

  _updateCamera();
}

void QIrrlichtWidget::wheelEvent(QWheelEvent* event)
{
  const double zoomFactor = 1;

  _distance += zoomFactor * event->delta() / 120.0;

  _updateCamera();
}

void QIrrlichtWidget::_updateCamera()
{
  if (!device->getSceneManager()->getActiveCamera())
  {
    camera = device->getSceneManager()->addCameraSceneNode(
        0
        , vector3df(0, 30, -40)
        , vector3df(0, 5, 0));

    _updateSize(size());
    updateIrrlicht(device);
  }

  if (_distance < _minDistance)
    _distance = _minDistance;

  if (_distance > _maxDistance)
    _distance = _maxDistance;

  if (_pitch <= 0.0001)
    _pitch = 0.0001;

  if (_pitch > M_PI - 0.0001)
    _pitch = M_PI - 0.0001;

  double positionX = _lookAt[0] + _distance * cos(_yaw) * sin(_pitch);
  double positionY = _lookAt[1] + _distance * cos(_pitch);
  double positionZ = _lookAt[2] + _distance * sin(_yaw) * sin(_pitch);

  camera->setPosition(core::vector3df(positionX, positionY, positionZ));
  camera->setTarget(core::vector3df(_lookAt[0], _lookAt[1], _lookAt[2]));
}

// Include the extra Qt file for signals and slots
// #include "moc_QIrrlichtWidget.cpp"
