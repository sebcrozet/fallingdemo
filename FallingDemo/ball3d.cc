#include "ball3d.hh"

namespace Test
{
  Ball3d::Ball3d(World3d& world, ISceneManager* manager)
    : Ball3d(world
             , manager
             , 1000.0 * (double)rand() / (double)RAND_MAX
             , 1000.0 * (double)rand() / (double)RAND_MAX
             , 1000.0 * (double)rand() / (double)RAND_MAX)
  {
  }

  Ball3d::Ball3d(World3d& world, ISceneManager* manager, double x, double y, double z)
    : SceneNode3d(manager)
  {
    auto br = 0.25 + ((double)rand() / (double)RAND_MAX) * 0.25; // radius for the rigid body

    int cr = std::min(255, (int)((double)rand() / (double)RAND_MAX * (double)255));
    int cg = std::min(255, (int)((double)rand() / (double)RAND_MAX * (double)255));
    int cb = std::min(255, (int)((double)rand() / (double)RAND_MAX * (double)255));

    _sceneNode = manager->addSphereSceneNode(br);
    _sceneNode->setPosition(vector3df(x, y, z));
    _sceneNode->setMaterialFlag(EMF_LIGHTING, true);
    _sceneNode->setMaterialFlag(EMF_BACK_FACE_CULLING, false);
    _sceneNode->getMaterial(0).Shininess = 2.0f;
    _sceneNode->getMaterial(0).SpecularColor.set(255, cr, cg, cb);
    _sceneNode->getMaterial(0).AmbientColor.set(255, cr, cg, cb); 
    _sceneNode->getMaterial(0).DiffuseColor.set(255, cr, cg, cb); // 255,255,255,255); 
    _sceneNode->getMaterial(0).EmissiveColor.set(0,0,0,0);

    _shape   = new Falling3d::Ball3d(br);
    _physics = new Body3d(_shape, true);
    translate(Vect3d(x, y, z), *_physics);

    world.addBody(_physics);
  }

  Ball3d::~Ball3d()
  {
    delete _shape;
  }
} // end Test
