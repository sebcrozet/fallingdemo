#include "box.hh"
#include "Constants.hh"

namespace Test
{
  Box::Box(Falling2d::World2d& world)
    : Box(world
          , 1000.0 * (double)rand() / (double)RAND_MAX
          , 700.0 * (double)rand() / (double)RAND_MAX)
  { }

  Box::Box(Falling2d::World2d& world, double x, double y)
  {
    using namespace Falling2d;

    auto rx = 0.25 + ((double)rand() / (double)RAND_MAX) * 0.25; // radius for the rigid body
    auto ry = 0.25 + ((double)rand() / (double)RAND_MAX) * 0.25; // radius for the rigid body
    auto drx  = rx * DRAW_SCALE; // radius for the display
    auto dry  = ry * DRAW_SCALE; // radius for the display
    auto cr = ((double)rand() / (double)RAND_MAX) * 100;
    auto cg = ((double)rand() / (double)RAND_MAX) * 100;
    auto cb = ((double)rand() / (double)RAND_MAX) * 100;

    _image.setSize(sf::Vector2f(drx * 2.0, dry * 2.0));
    _image.setFillColor(sf::Color(cr, cg, cb));
    _image.setOrigin(drx, dry);
    _image.setOutlineThickness(THICKNESS);
    _image.setOutlineColor(sf::Color(cr + 100, cg + 100, cb + 100));

    _shape = new Box2d({rx, ry});
    _body  = new Body2d(_shape, true);
    translate(Vect2d(x / DRAW_SCALE, y / DRAW_SCALE), *_body);
    world.addBody(_body);
  }

  void Box::draw(sf::RenderWindow& rw)
  {
    auto pos = translation(*_body);
    _image.setPosition((float)pos.at(0) * DRAW_SCALE,
                       (float)pos.at(1) * DRAW_SCALE);
    rw.draw(_image);
  }

  Falling2d::Body2d* Box::physics()
  {
    return _body;
  }

  Box::~Box()
  {
    delete _body;
    delete _shape;
  }
} // end Test
